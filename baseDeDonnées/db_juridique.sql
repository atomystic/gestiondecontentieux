-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 18 jan. 2021 à 18:14
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `db_juridique`
--

-- --------------------------------------------------------

--
-- Structure de la table `affaires`
--

CREATE TABLE `affaires` (
  `id_a` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `statut` int(11) NOT NULL,
  `titre` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `affaires`
--

INSERT INTO `affaires` (`id_a`, `description`, `reference`, `statut`, `titre`) VALUES
(5, 'Le 4 juillet 2018, le Royaume d’Arabie saoudite, le Royaume de Bahreïn, la République arabe d’Egypte et les Emirats arabes unis ont déposé auprès du greffe de la Cour une requête conjointe par laquelle ils faisaient appel d’une décision rendue le 29 juin ', '345', 1, 'Appel concernant la compétence du Conseil de l’OACI en vertu de l’article 84 de la convention relative à l’aviation civile internationale (Arabie saoudite, Bahreïn, Egypte et Emirats arabes unis c. Qatar)'),
(4, 'Le 13 juin 2016, la République de Guinée équatoriale a introduit une instance contre la République française au sujet d’un différend ayant trait à « l’immunité de juridiction pénale du second vice-président chargé de la défense et de la sécurité de l’Etat', '123', 3, 'Immunités et procédures pénales'),
(6, 'Le 8 mai 2017, l’Inde a déposé une requête introductive d’instance contre le Pakistan au sujet d’un différend concernant les violations alléguées de la Convention de Vienne sur les relations consulaires du 24 avril 1963 qui auraient été commises «dans le ', '789', 1, 'Jadhav (Inde c. Pakistan)'),
(7, 'Le 22 juin 2017, l’Assemblée générale a adopté la résolution 71/292, dans laquelle, se référant à l’article 65 du Statut de la Cour, elle a prié celle-ci de donner un avis consultatif sur les questions suivantes:', '101112', 2, 'Effets juridiques de la séparation de l’archipel des Chagos de Maurice en 1965');

-- --------------------------------------------------------

--
-- Structure de la table `documents`
--

CREATE TABLE `documents` (
  `id_d` bigint(20) NOT NULL,
  `date_creation` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `a_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `documents`
--

INSERT INTO `documents` (`id_d`, `date_creation`, `description`, `nom`, `a_id`) VALUES
(5, '2021-01-17 16:52:51', 'je suis là', 'je suis là', 4),
(6, '2021-01-18 18:09:38', 'je suis là description de sas', 'Création de sas', 5),
(7, '2021-01-18 18:10:20', 'je suis la création de SARL', 'Création de SARL', 6),
(8, '2021-01-18 18:11:02', 'je suis le SCI', 'SCI', 5);

-- --------------------------------------------------------

--
-- Structure de la table `phases`
--

CREATE TABLE `phases` (
  `id_p` bigint(20) NOT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `ta_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `phases`
--

INSERT INTO `phases` (`id_p`, `date_debut`, `date_fin`, `nom`, `ta_id`) VALUES
(5, '2021-01-17 16:55:00', '2021-01-17 19:55:00', 'erer', 4),
(6, '2021-02-27 18:05:00', '2021-02-28 18:05:00', 'je suis la phase 1', 5),
(7, '2021-03-18 18:06:00', '2021-01-22 18:06:00', 'je suis là phase 2', 4),
(8, '2021-04-06 18:06:00', '2021-01-21 18:07:00', 'je suis là tâche 5', 6);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'ROLE_ADMIN'),
(3, 'ROLE_JURIDIQUE');

-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

CREATE TABLE `tache` (
  `id_ta` bigint(20) NOT NULL,
  `date_creation` datetime DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `statut_audience` bit(1) NOT NULL,
  `titre` varchar(500) DEFAULT NULL,
  `a_id` bigint(20) DEFAULT NULL,
  `t_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tache`
--

INSERT INTO `tache` (`id_ta`, `date_creation`, `description`, `statut_audience`, `titre`, `a_id`, `t_id`) VALUES
(4, '2021-01-17 16:54:37', 'je suis la', b'1', 'je suis la', 4, 1),
(5, '2021-01-18 18:03:12', 'je suis là description de la tâche 1', b'1', 'je suis là tâche 1', 4, 1),
(6, '2021-01-18 18:04:30', 'je suis là description de la tâche 3', b'1', 'je suis là tâche 3', 7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tache_utilisateur`
--

CREATE TABLE `tache_utilisateur` (
  `ta_id` bigint(20) NOT NULL,
  `id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tache_utilisateur`
--

INSERT INTO `tache_utilisateur` (`ta_id`, `id`) VALUES
(4, 12),
(6, 13),
(5, 13);

-- --------------------------------------------------------

--
-- Structure de la table `tribunal`
--

CREATE TABLE `tribunal` (
  `id_t` bigint(20) NOT NULL,
  `adresse` varchar(500) DEFAULT NULL,
  `fax` double DEFAULT NULL,
  `region` varchar(500) DEFAULT NULL,
  `tel` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tribunal`
--

INSERT INTO `tribunal` (`id_t`, `adresse`, `fax`, `region`, `tel`) VALUES
(1, '5 avenue henry becquerel', 10.1, 'Nouvelle-Aquitaine', 555656585);

-- --------------------------------------------------------

--
-- Structure de la table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(3, 1),
(4, 1),
(5, 3),
(8, 1),
(9, 1),
(10, 1),
(13, 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `user_id` bigint(20) NOT NULL,
  `email` varchar(500) DEFAULT NULL,
  `mot_de_passe` varchar(500) DEFAULT NULL,
  `nom_utilisateur` varchar(500) DEFAULT NULL,
  `prenom_utilisateur` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`user_id`, `email`, `mot_de_passe`, `nom_utilisateur`, `prenom_utilisateur`) VALUES
(13, 'gnavo.tony@hotmail.fr', '$2a$10$n4XMcb3oFcgUc9NO9hjL8.ZkiyKCbyyJPGDFhDLOJqXKAauaW5nSe', 'atomystic', 'Tony');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `affaires`
--
ALTER TABLE `affaires`
  ADD PRIMARY KEY (`id_a`);

--
-- Index pour la table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id_d`),
  ADD KEY `FK5bl6y9ng2eyl07h2u9bd8pp76` (`a_id`);

--
-- Index pour la table `phases`
--
ALTER TABLE `phases`
  ADD PRIMARY KEY (`id_p`),
  ADD KEY `FK2g0ngwbx9dstk2sog59pifk6i` (`ta_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tache`
--
ALTER TABLE `tache`
  ADD PRIMARY KEY (`id_ta`),
  ADD KEY `FKghdu54cor4ixtvmu7gmm5bo5y` (`a_id`),
  ADD KEY `FKrws8twk2e1cptf7a3lhwarr1f` (`t_id`);

--
-- Index pour la table `tache_utilisateur`
--
ALTER TABLE `tache_utilisateur`
  ADD KEY `FKjc96no4ud004x2uh6yeh18cb` (`id`),
  ADD KEY `FKd3ak33nishqlm86sug8kf5twy` (`ta_id`);

--
-- Index pour la table `tribunal`
--
ALTER TABLE `tribunal`
  ADD PRIMARY KEY (`id_t`);

--
-- Index pour la table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `UKni8mpsewdiqihltm1w18c8dix` (`nom_utilisateur`) USING HASH,
  ADD UNIQUE KEY `UKrma38wvnqfaf66vvmi57c71lo` (`email`) USING HASH;

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `affaires`
--
ALTER TABLE `affaires`
  MODIFY `id_a` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `documents`
--
ALTER TABLE `documents`
  MODIFY `id_d` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `phases`
--
ALTER TABLE `phases`
  MODIFY `id_p` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tache`
--
ALTER TABLE `tache`
  MODIFY `id_ta` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `tribunal`
--
ALTER TABLE `tribunal`
  MODIFY `id_t` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
