package com.tony.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tony.entities.Affaire;
import com.tony.service.IAffaireService;

@RestController
@CrossOrigin(value = "*")
@RequestMapping(value = "/affaire")
public class AffaireController {

	@Autowired
	IAffaireService iAffaireService;
	
	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@GetMapping(value = "/findAllAffaire", produces = "application/json")
	List<Affaire> findAllAffaires() {

		return iAffaireService.findAllAffaires();
	}

	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@GetMapping(value = "/findAffaireById/{pId}", produces = "application/json")
	Affaire finById(@PathVariable("pId") Long idAffaire) {

		return iAffaireService.findAffaireById(idAffaire);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/updateAffaire",produces = "application/json")
	Affaire updateAffaire(@RequestBody Affaire affaire) {

		return iAffaireService.updateAffaire(affaire);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/deleteAffaire/{id}")
	void deleteAffaire(@PathVariable("id")Long id) {
		
		iAffaireService.deleteAffaire(id);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(value = "/addAffaire")
	public Affaire addAffaire(@RequestBody Affaire affaire) {
		
		
		return iAffaireService.addAffaire(affaire);
	}
	
}
