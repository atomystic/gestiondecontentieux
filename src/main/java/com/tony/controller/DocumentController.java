package com.tony.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tony.entities.Affaire;
import com.tony.entities.Document;
import com.tony.service.IAffaireService;
import com.tony.service.IDocumentService;

@RestController
@RequestMapping(value ="/document")
@CrossOrigin(origins = "*")
public class DocumentController {

	@Autowired
	IDocumentService iDocumentService;
	@Autowired
	IAffaireService iAffaireService;
	
	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@PostMapping(value = "/addDocument/{idAffaire}")
    public Document addDocument( @PathVariable("idAffaire") long id,@RequestBody Document document) {
		
		
        Affaire affaire = iAffaireService.findAffaireById(id);
        document.setAffaireD(affaire);
        document.setDateCreation(LocalDateTime.now());
    	return iDocumentService.addDocument(document);
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/deleteDocument/{id}")
	void deleteDocument(@PathVariable("id")Long id) {
		
		iDocumentService.deleteDocument(id);
	}
	

	
}
