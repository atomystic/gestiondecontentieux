package com.tony.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tony.entities.Phase;
import com.tony.entities.Tache;
import com.tony.service.IPhaseService;
import com.tony.service.ITacheService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/phase")
public class PhaseController {
	@Autowired
	IPhaseService iPhaseService;
	@Autowired
	ITacheService iTacheService;
	
	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@GetMapping(value = "/findAllPhases",produces = "application/json")
	List<Phase> findAllPhases(){
		
		return iPhaseService.findAllPhases();
	}
	
	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@PostMapping(value = "/addPhaseWithTache/{idTache}")
	Phase addPhaseWithTache(@RequestBody Phase phase , @PathVariable("idTache")Long idTache) {
		
		Tache tache = iTacheService.findTacheById(idTache);
		phase.setTache(tache);
		return iPhaseService.addPhase(phase);
		
	}
	
	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@DeleteMapping(value = "/deletePhase/{id}")
	void deletePhase (@PathVariable("id")Long id) {
		
		iPhaseService.deletePhase(id);
		
		
	}
	

}
