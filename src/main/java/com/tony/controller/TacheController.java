package com.tony.controller;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tony.entities.Tache;
import com.tony.service.ITacheService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/tache")
public class TacheController {

	@Autowired
	ITacheService iTacheService;

	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@GetMapping(value = "/findAllTaches", produces = "application/json")
	List<Tache> findAllTaches() {
		return iTacheService.findAllTache();
	}
	

	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@GetMapping(value = "/findTacheById/{pId}", produces = "application/json")
	Tache findTacheById(@PathVariable("pId") Long idTache) {
		
		return iTacheService.findTacheById(idTache);
	}

	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@PostMapping(value = "/addTache")
	Tache addTache(@RequestBody Tache tache) {
	tache.setDateCreation(ZonedDateTime.now());
	return iTacheService.addTache(tache);
	}


	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@PutMapping(value = "/updateTache")
	Tache updateTache(@RequestBody Tache tache) {
	return iTacheService.addTache(tache);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/deleteTache/{id}")
	void deleteTache(@PathVariable("id") Long id) {
		
		
		iTacheService.deleteTache(id);
	}

}
