package com.tony.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JacksonInject.Value;
import com.tony.entities.Tribunal;
import com.tony.service.ITribunalService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/tribunal")
public class TribunalController {
	
	@Autowired
	ITribunalService iTribunalService;
	
	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@GetMapping(value ="/findAllTribunal")
	List<Tribunal> findAllTribunal(){
		
		return iTribunalService.findAllTribunal();
	}

}
