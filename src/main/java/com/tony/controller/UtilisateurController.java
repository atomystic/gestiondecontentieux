package com.tony.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tony.entities.Utilisateur;
import com.tony.service.IUtilisateurService;

@RestController
@RequestMapping("/utilisateur")
@CrossOrigin(origins = "*")
public class UtilisateurController {

	@Autowired
	IUtilisateurService iUtilisateur;
	@Autowired
	PasswordEncoder encoder;
	
	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@GetMapping(value = "/findAllUtilisateurs")
	public List<Utilisateur> findAllUtilisateurs(){
		
		
		return  iUtilisateur.findAllUtilisateurs();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/deleteUtilisateur/{id}")
	void deleteUtilisateur(@PathVariable("id") Long id) {
		
		iUtilisateur.deleteUtilisateur(id);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping(value = "/findUtilisateur/{id}")
	Utilisateur findUtilisateur (@PathVariable("id")Long id) {
		
		return iUtilisateur.findUtilisateur(id);
	}
	
	@PreAuthorize("hasRole('JURIDIQUE') or hasRole('ADMIN')")
	@PutMapping(value = "/updateUtilisateur")
	void updateUtilisateur(@RequestBody Utilisateur utilisateur) {
		
		String mdp = encoder.encode(utilisateur.getMotDePasse());
		utilisateur.setMotDePasse(mdp);	
		

	 iUtilisateur.updateUtilisateur(utilisateur);
		
	}

}
