package com.tony.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table (name = "affaires")
public class Affaire implements Serializable {

	// déclaration des attribute
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column (name = "id_a")
	private Long idAffaire;
	private String reference;
	private String titre;
	@Size(max = 500)
	private String description;
	private int statut;
	
	@JsonIgnore
	@OneToMany (mappedBy = "affaire",cascade = CascadeType.REMOVE)
	private List<Tache> taches;

	@OneToMany (mappedBy = "affaireD", cascade = CascadeType.REMOVE,fetch = FetchType.EAGER)
	private List<Document> documents;
	// constructeurs
	public Affaire() {
		super();
	}
	public Affaire(Long idAffaire, String reference, String titre, String description, int statut) {
		super();
		this.idAffaire = idAffaire;
		this.reference = reference;
		this.titre = titre;
		this.description = description;
		this.statut = statut;
	}
	
	// getters et setters
	public Affaire(String reference, String titre, String description, int statut) {
		super();
		this.reference = reference;
		this.titre = titre;
		this.description = description;
		this.statut = statut;
	}
	public Long getIdAffaire() {
		return idAffaire;
	}
	public void setIdAffaire(Long idAffaire) {
		this.idAffaire = idAffaire;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStatut() {
		return statut;
	}
	public void setStatut(int statut) {
		this.statut = statut;
	}
	
	public List<Tache> getTaches() {
		return taches;
	}
	public void setTaches(List<Tache> taches) {
		this.taches = taches;
	}
	
	public List<Document> getDocuments() {
		return documents;
	}
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	@Override
	public String toString() {
		return "Affaire [idAffaire=" + idAffaire + ", reference=" + reference + ", titre=" + titre + ", description="
				+ description + ", statut=" + statut + "]";
	}
	
	

}
