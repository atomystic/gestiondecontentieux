package com.tony.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table (name = "documents")
public class Document implements Serializable {
	
	// déclarations des attributs
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id_d")
	private Long idDocument;
	private LocalDateTime dateCreation;
	private String nom;
	private String description;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn (name = "a_id",referencedColumnName ="id_a")
	private Affaire affaireD;
	
	// constructeurs
	public Document() {
		super();
	}

	public Document(Long idDocument, LocalDateTime dateCreation, String nom, String description) {
		super();
		this.idDocument = idDocument;
		this.dateCreation = dateCreation;
		this.nom = nom;
		this.description = description;
	}

	public Document(LocalDateTime dateCreation, String nom, String description) {
		super();
		this.dateCreation = dateCreation;
		this.nom = nom;
		this.description = description;
	}

	public Long getIdDocument() {
		return idDocument;
	}

	public void setIdDocument(Long idDocument) {
		this.idDocument = idDocument;
	}

	public LocalDateTime getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Affaire getAffaireD() {
		return affaireD;
	}

	public void setAffaireD(Affaire affaireD) {
		this.affaireD = affaireD;
	}

	@Override
	public String toString() {
		return "Document [idDocument=" + idDocument + ", dateCreation=" + dateCreation + ", nom=" + nom
				+ ", description=" + description + "]";
	}
	
	
	

	
	

}
