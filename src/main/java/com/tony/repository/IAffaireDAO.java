package com.tony.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tony.entities.Affaire;
import com.tony.entities.Tache;

@Repository
public interface IAffaireDAO  extends JpaRepository<Affaire,Long> {


}