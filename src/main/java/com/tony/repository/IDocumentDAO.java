package com.tony.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tony.entities.Document;

@Repository
public interface IDocumentDAO extends JpaRepository<Document,Long> {

}
