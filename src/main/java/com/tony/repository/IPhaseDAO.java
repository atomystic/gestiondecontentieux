package com.tony.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tony.entities.Phase;

@Repository
public interface IPhaseDAO extends JpaRepository<Phase,Long> {

	@Query(value = 	"SELECT * FROM phases WHERE ta_id = :i",nativeQuery = true)
	List<Phase> recupererPhase(@Param("i") Long idTache);

}
