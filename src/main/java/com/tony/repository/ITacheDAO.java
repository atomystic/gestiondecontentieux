package com.tony.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tony.entities.Tache;

@Repository
public interface ITacheDAO extends JpaRepository<Tache, Long> {

//	@Query(value = "INSERT INTO tache_utilisateur (ta_id,id) VALUES (:ta_id,:id)", nativeQuery = true)
//	void addUserToTask(@Param("ta_id") Long idTache, @Param("id") Long idUtilisateur);
}
