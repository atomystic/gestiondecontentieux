package com.tony.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.tony.entities.Utilisateur;

@Repository
public interface IUtilisateurDAO extends JpaRepository<Utilisateur,Long> {

	@Modifying
	@Transactional
	@Query(value = "UPDATE Utilisateur u SET u.email=:e, u.nomUtilisateur=:n, u.prenomUtilisateur =:p, u.motDePasse =:m WHERE u.idUtilisateur=:i")
	void updateUtilisateur (@Param("e")String email,@Param("n")String nom,@Param("p")String prenom, @Param("m")String mdp , @Param("i")Long id);
}
