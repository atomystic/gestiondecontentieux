package com.tony.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tony.entities.Affaire;
import com.tony.repository.IAffaireDAO;

@Service
public class AffaireService implements IAffaireService {

	@Autowired
	IAffaireDAO iAffaireDAO;

	@Override
	public List<Affaire> findAllAffaires() {

		return iAffaireDAO.findAll();
	}

	@Override
	public Affaire findAffaireById(Long idAffaire) {
		
		return iAffaireDAO.findById(idAffaire).get();
	}

	@Override
	public Affaire updateAffaire(Affaire affaire) {
		
		return iAffaireDAO.save(affaire);
	}

	@Override
	public void deleteAffaire(Long id) {
		iAffaireDAO.deleteById(id);
		
	}

	@Override
	public Affaire addAffaire(Affaire affaire) {
		
		return iAffaireDAO.save(affaire);
	}

}
