package com.tony.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tony.entities.Document;
import com.tony.repository.IDocumentDAO;

@Service
public class DocumentService implements IDocumentService {
	
	@Autowired
	IDocumentDAO iDocumentDAO;

	@Override
	public Document addDocument(Document document) {
		
		return iDocumentDAO.save(document);
	}

	@Override
	public void deleteDocument(Long id) {
		 iDocumentDAO.deleteById(id);
		
	}

}
