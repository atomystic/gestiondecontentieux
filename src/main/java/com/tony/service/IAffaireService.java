package com.tony.service;

import java.util.List;

import com.tony.entities.Affaire;

public interface IAffaireService {

List<Affaire> findAllAffaires();
Affaire findAffaireById(Long idAffaire);
Affaire updateAffaire(Affaire affaire);
void deleteAffaire(Long id);
Affaire addAffaire (Affaire affaire);
}
