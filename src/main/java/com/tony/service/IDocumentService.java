package com.tony.service;

import com.tony.entities.Document;

public interface IDocumentService {
	
	Document addDocument(Document document);
	void deleteDocument(Long id);

}
