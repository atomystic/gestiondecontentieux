package com.tony.service;

import java.util.List;

import com.tony.entities.Phase;

public interface IPhaseService {
	
 List<Phase>findAllPhases();
 Phase addPhase(Phase phase);
  void deletePhase(Long id);

}
