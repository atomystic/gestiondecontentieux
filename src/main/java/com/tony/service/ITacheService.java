package com.tony.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tony.entities.Tache;


public interface ITacheService {

	List<Tache> findAllTache();
	 Tache findTacheById(Long idTache);
	 Tache addTache(Tache tache);
	 void deleteTache(Long id);
}
