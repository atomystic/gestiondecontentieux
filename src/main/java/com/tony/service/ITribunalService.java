package com.tony.service;

import java.util.List;

import com.tony.entities.Tribunal;

public interface ITribunalService {
	
	List<Tribunal> findAllTribunal();

}
