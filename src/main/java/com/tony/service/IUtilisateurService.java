package com.tony.service;

import java.util.List;

import com.tony.entities.Utilisateur;

public interface IUtilisateurService {
	
	
	List<Utilisateur> findAllUtilisateurs();
	void deleteUtilisateur(Long id);
	Utilisateur findUtilisateur(Long id);
	void updateUtilisateur (Utilisateur utilisateur);
}
