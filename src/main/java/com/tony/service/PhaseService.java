package com.tony.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tony.entities.Phase;
import com.tony.repository.IPhaseDAO;

@Service
public class PhaseService implements IPhaseService {
	@Autowired
	IPhaseDAO iPhaseDAO;

	@Override
	public List<Phase> findAllPhases() {

		return iPhaseDAO.findAll();
	}

	@Override
	public Phase addPhase(Phase phase) {
		
		return iPhaseDAO.save(phase);
	}

	@Override
	public void deletePhase(Long id) {
		iPhaseDAO.deleteById(id);
		
	}

}
