package com.tony.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tony.entities.Tache;
import com.tony.repository.ITacheDAO;

@Service
public class TacheService implements ITacheService {

	@Autowired
	ITacheDAO iTacheDAO;

	@Override
	public List<Tache> findAllTache() {

		return iTacheDAO.findAll();
	}

	@Override
	public Tache findTacheById(Long idTache) {
		
		return iTacheDAO.findById(idTache).get();
	}

	@Override
	public Tache addTache(Tache tache) {
		
		return iTacheDAO.save(tache);
	}

	@Override
	public void deleteTache(Long id) {
	
		iTacheDAO.deleteById(id);
		
	}

}
