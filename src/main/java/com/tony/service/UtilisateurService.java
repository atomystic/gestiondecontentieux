package com.tony.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tony.entities.Utilisateur;
import com.tony.repository.IUtilisateurDAO;

@Service
public class UtilisateurService implements IUtilisateurService {

	@Autowired
	IUtilisateurDAO iUtilisateurDAO;

	@Override
	public List<Utilisateur> findAllUtilisateurs() {
		
		return iUtilisateurDAO.findAll();
	}

	@Override
	public void deleteUtilisateur(Long id) {
	iUtilisateurDAO.deleteById(id);
		
	}

	@Override
	public Utilisateur findUtilisateur(Long id) {
		
		return iUtilisateurDAO.findById(id).get();
	}

	@Override
	public void updateUtilisateur(Utilisateur u) {
		System.out.println("*********************************"+u+"*******************************************");
		iUtilisateurDAO.updateUtilisateur(u.getEmail(),u.getNomUtilisateur(),u.getPrenomUtilisateur(),u.getMotDePasse(),u.getIdUtilisateur());
		
	}


}
