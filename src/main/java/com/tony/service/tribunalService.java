package com.tony.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tony.entities.Tribunal;
import com.tony.repository.ITribunalDAO;

@Service
public class tribunalService implements ITribunalService {

	@Autowired
	ITribunalDAO iTribunalDAO;
	@Override
	public List<Tribunal> findAllTribunal() {
		
		return iTribunalDAO.findAll();
	}

}
