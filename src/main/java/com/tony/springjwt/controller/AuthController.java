package com.tony.springjwt.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tony.entities.ERole;
import com.tony.entities.Role;
import com.tony.entities.Utilisateur;
import com.tony.springjwt.payload.request.LoginRequest;
import com.tony.springjwt.payload.request.SignupRequest;
import com.tony.springjwt.payload.response.JwtResponse;
import com.tony.springjwt.payload.response.MessageResponse;
import com.tony.springjwt.repository.RoleRepository;
import com.tony.springjwt.repository.UserRepository;
import com.tony.springjwt.security.jwt.JwtUtils;
import com.tony.springjwt.security.service.UserDetailsImpl;

@RestController
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping(value = "/auth")
public class AuthController {
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;
	
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());
		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles, userDetails.getName()));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		
		if (userRepository.existsByNomUtilisateur(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Erreur:le nom d'utilisateur est déjà utilisé"));
		}

		
		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Erreur:l'email est déjà utilisé"));
		}


		// création de l'utilisateur
		Utilisateur user = new Utilisateur(signUpRequest.getEmail(), 
							 signUpRequest.getUsername(),
							 signUpRequest.getName(),
							 encoder.encode(signUpRequest.getPassword()));

		
		Set<String> strRoles = signUpRequest.getRole();
		System.out.println(strRoles);
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_ADMIN)
					.orElseThrow(() -> new RuntimeException("Erreur:le Role n'a pas été trouvé"));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Erreur:le Role n'a pas été trouvé"));
					roles.add(adminRole);
					break;
				case "jur":
					Role jurRole = roleRepository.findByName(ERole.ROLE_JURIDIQUE)
							.orElseThrow(() -> new RuntimeException("Erreur:le Role n'a pas été trouvé"));
					roles.add(jurRole);

					break;
					
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_JURIDIQUE)
							.orElseThrow(() -> new RuntimeException("Erreur:le Role n'a pas été trouvé"));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("Erreur:le Role n'a pas été trouvé"));
	}
	

}
