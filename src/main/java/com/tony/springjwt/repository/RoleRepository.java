package com.tony.springjwt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tony.entities.ERole;
import com.tony.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
	
	Optional<Role> findByName(ERole name);

}
