package com.tony.springjwt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tony.entities.Utilisateur;

@Repository
public interface UserRepository extends JpaRepository<Utilisateur, Long> {

	Optional<Utilisateur> findByNomUtilisateur(String username);

	Boolean existsByNomUtilisateur(String username);

	Boolean existsByEmail(String mail);

}
