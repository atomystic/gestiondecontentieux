package com.tony.springjwt.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tony.entities.Utilisateur;
import com.tony.springjwt.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Utilisateur user = userRepository.findByNomUtilisateur(username)
				.orElseThrow(() -> new UsernameNotFoundException("L'utilisateur " + username + " n'a pas été trouvé"));

		return UserDetailsImpl.build(user);

	}

}
