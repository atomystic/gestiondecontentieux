package com.tony.dao;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tony.entities.Affaire;
import com.tony.entities.Document;
import com.tony.repository.IAffaireDAO;
import com.tony.repository.IDocumentDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class IAffaireDAOTest {
	@Autowired
	IAffaireDAO iAffaireDAO;
	@Autowired
	IDocumentDAO iDocumentDAO;
	Affaire affaire;

	@BeforeEach
	void initialiserAffaire() {
		affaire = new Affaire("reference", "titre", "description", 1);
	}

	@Test
	void ajouterAffaire1() {
		int tailleListeAffairesAvant = iAffaireDAO.findAll().size();
		Affaire affaireSave = iAffaireDAO.save(affaire);
		int tailleListeAffairesAprès = iAffaireDAO.findAll().size();
		assertEquals(tailleListeAffairesAvant + 1, tailleListeAffairesAprès);
//		iAffaireDAO.deleteById(affaireSave.getIdAffaire());

	}

	@Test
	void supprimerAffaire2() {

		Affaire affaireSave = iAffaireDAO.save(affaire);
		int tailleListeAffairesAvant = iAffaireDAO.findAll().size();
		iAffaireDAO.deleteById(affaireSave.getIdAffaire());
		int tailleListeAffairesAprès = iAffaireDAO.findAll().size();
		assertEquals(tailleListeAffairesAvant - 1, tailleListeAffairesAprès);

	}

	@Test
	void recupererAffaire3() {

		Affaire affaireSave = iAffaireDAO.save(affaire);
		Affaire affaireRecuperer = iAffaireDAO.findById(affaireSave.getIdAffaire()).get();
		assertNotEquals("null", affaireRecuperer);
//		iAffaireDAO.deleteById(affaireRecuperer.getIdAffaire());

	}

	@Test
	void recupererAffaireAvecDocuments4() {
		Document document = new Document(LocalDateTime.now(), "nom", ""
				+ "description");
		Affaire affaireSave = iAffaireDAO.save(affaire);
		document.setAffaireD(affaireSave);
		iDocumentDAO.save(document);
		Affaire affaireRecupAvecDocuments = iAffaireDAO.findById(affaireSave.getIdAffaire()).get();
		assertEquals("nom", affaireRecupAvecDocuments.getDocuments().get(0).getNom());
//		iAffaireDAO.deleteById(affaireSave.getIdAffaire());

	}

}
