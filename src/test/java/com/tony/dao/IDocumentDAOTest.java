package com.tony.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.junit.FixMethodOrder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tony.entities.Affaire;
import com.tony.entities.Document;
import com.tony.entities.Tribunal;
import com.tony.repository.IAffaireDAO;
import com.tony.repository.IDocumentDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class IDocumentDAOTest {

	@Autowired
	IDocumentDAO iDocumentDAO;
	private Document document;
	@Autowired
	IAffaireDAO iAffaireDAO;

	@BeforeEach
	public void instanciationAffaire() {
		document = new Document(LocalDateTime.now(), "nom", "description");

	}

	@Test
	public void ajouterDocumentSansAffaire1() {
		int tailleListeAffairesAvant = iDocumentDAO.findAll().size();
		Document documentSave = iDocumentDAO.save(document);
		int tailleListeAffairesApres = iDocumentDAO.findAll().size();
		assertEquals(tailleListeAffairesAvant + 1, tailleListeAffairesApres);
//		iDocumentDAO.deleteById(documentSave.getIdDocument());

	}

	@Test
	public void ajouterDocumentAvecAffaire2() {
		
		Affaire affaireSave = iAffaireDAO.save(new Affaire("reference", "titre", "description", 1));
		Affaire affaireRecup = iAffaireDAO.findById(affaireSave.getIdAffaire()).get();
		int tailleListeDocumentAvant = iDocumentDAO.findAll().size();
		document.setAffaireD(affaireRecup);
		Document documentSave = iDocumentDAO.save(document);
		int tailleListeDocumentApres = iDocumentDAO.findAll().size();
		assertEquals(tailleListeDocumentAvant + 1, tailleListeDocumentApres);
//		iDocumentDAO.deleteById(documentSave.getIdDocument());

	}
	
	@Test
	void supprimerDocument3() {

		Document documentSave = iDocumentDAO.save(document);
		int tailleListeDocumentsAvant = iDocumentDAO.findAll().size();
		iDocumentDAO.deleteById(documentSave.getIdDocument());
		int tailleListeDocumentsAprès = iDocumentDAO.findAll().size();
		assertEquals(tailleListeDocumentsAvant - 1, tailleListeDocumentsAprès);

	}
}
