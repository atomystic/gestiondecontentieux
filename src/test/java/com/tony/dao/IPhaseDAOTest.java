package com.tony.dao;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.ZonedDateTime;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tony.entities.Phase;
import com.tony.entities.Tache;
import com.tony.repository.IPhaseDAO;
import com.tony.repository.ITacheDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class IPhaseDAOTest {

	@Autowired
	IPhaseDAO iphaseDAO;
	@Autowired
	ITacheDAO iTacheDAO;
	Phase phase;

	@BeforeEach
	public void instanciationPhase() {
		phase = new Phase("phase1", ZonedDateTime.now(), ZonedDateTime.now());

	}

	@Test
	public void ajouterPhaseSansTache1() {
		int tailleListePhasesAvant = iphaseDAO.findAll().size();
		Phase phaseSave = iphaseDAO.save(phase);
		int tailleListePhasesApres = iphaseDAO.findAll().size();
		assertEquals(tailleListePhasesAvant + 1, tailleListePhasesApres);
//		iphaseDAO.deleteById(phaseSave.getIdPhase());

	}

	@Test
	public void ajouterPhaseAvecTache2() {
		Tache tacheSave = iTacheDAO.save(new Tache(ZonedDateTime.now(), "titre", "description", true));
		Tache tacheRecup = iTacheDAO.findById(tacheSave.getIdTache()).get();
		int tailleListePhaseAvant = iphaseDAO.findAll().size();
		phase.setTache(tacheRecup);
		Phase phaseSave = iphaseDAO.save(phase);
		int tailleListePhaseApres = iphaseDAO.findAll().size();
		assertEquals(tailleListePhaseAvant + 1, tailleListePhaseApres);
//		iphaseDAO.deleteById(phaseSave.getIdPhase());

	}

	public void recupererPhaseAveTache() {
		Tache tacheSave = iTacheDAO.save(new Tache(ZonedDateTime.now(), "titre", "description", true));
		phase.setTache(tacheSave);
		Phase phaseSave = iphaseDAO.save(phase);
		assertNotEquals("null", phaseSave.getTache());
//		iphaseDAO.deleteById(phaseSave.getIdPhase());

	}

}
