package com.tony.dao;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tony.entities.Affaire;
import com.tony.entities.Phase;
import com.tony.entities.Tache;
import com.tony.entities.Tribunal;
import com.tony.entities.Utilisateur;
import com.tony.repository.IAffaireDAO;
import com.tony.repository.IPhaseDAO;
import com.tony.repository.ITacheDAO;
import com.tony.repository.ITribunalDAO;
import com.tony.repository.IUtilisateurDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ITacheDAOTest {

	@Autowired
	ITacheDAO iTacheDAO;
	@Autowired
	ITribunalDAO iTribunalDAO;
	@Autowired
	IAffaireDAO iAffaireDAO;
	@Autowired
	IUtilisateurDAO iUtilisateurDAO;
	@Autowired
	IPhaseDAO iphaseDAO;
	Tache tache;

	@BeforeEach
	void initialiserTache() {
		tache = new Tache(ZonedDateTime.now(), "Titre", "Description", true);
	}

	@Test
	void ajouterTacheSansRelation1() {

		int tailleAvantAjoutTache = iTacheDAO.findAll().size();
		Tache tacheSave = iTacheDAO.save(tache);
		int tailleApresAjoutTache = iTacheDAO.findAll().size();
		assertEquals(tailleAvantAjoutTache + 1, tailleApresAjoutTache);
//		iTacheDAO.deleteById(tacheSave.getIdTache());

	}

	@Test
	void supprimerTache2() {
		Tache tacheSave = iTacheDAO.save(tache);
		System.out.println("***************************"+iTacheDAO.findAll().size()+"*************************************");
		int tailleAvantSuppressionTache = iTacheDAO.findAll().size();
		iTacheDAO.deleteById(tacheSave.getIdTache());
		int tailleApresSuppressionTache1 = iTacheDAO.findAll().size();
		assertEquals(tailleAvantSuppressionTache - 1, tailleApresSuppressionTache1);

	}

	@Test
	void modifierTache3() {
		Tache tacheSave = iTacheDAO.save(tache);
		tacheSave.setTitre("Titre2");
		Tache tacheModifie = iTacheDAO.save(tacheSave);
		assertEquals("Titre2", tacheModifie.getTitre());
//		iTacheDAO.deleteById(tacheSave.getIdTache());
	}

	@Test
	void associerUtilisateurATache4() {

		/// Creation de las listes des utilisateurs à set dans la tache. l'ajout de
		/// l'utilisateurs se fait après l'ajout d'une tache dans l'application
		List<Utilisateur> utilisateurs = new ArrayList<>();
		Utilisateur utilisateurSave = iUtilisateurDAO
				.save(new Utilisateur("email", "nomUtilisateur", "prenomUtilisateur", "motDePasse"));
		utilisateurs.add(utilisateurSave);
		Tache tacheSave = iTacheDAO.save(tache);
		tacheSave.setUtilisateurs(utilisateurs);
		Tache tacheRecup = iTacheDAO.save(tacheSave);
		assertNotEquals("null", tacheRecup.getUtilisateurs().get(0).getNomUtilisateur());
		iTacheDAO.deleteById(tacheSave.getIdTache());
//		iUtilisateurDAO.deleteById(utilisateurSave.getIdUtilisateur());

	}

	// association du tribunal et de l'affaire des l'ajout de la tache
	@Test
	void TachesAvecTribunalEtAffaire5() {

		Tribunal tribunal = new Tribunal("adresse", 10.10, 10.10, "region");
		Tribunal tribunalSave = iTribunalDAO.save(tribunal);
		Affaire affaire = new Affaire("reference", "titre", "description", 10);
		Affaire affaireSave = iAffaireDAO.save(affaire);
		Tache tacheSave = iTacheDAO.save(tache);
		tacheSave.setTribunal(tribunalSave);
		tacheSave.setAffaire(affaireSave);
		Tache tacheRecup = iTacheDAO.save(tacheSave);
		assertNotEquals("null", tacheRecup.getAffaire());
		assertNotEquals("null", tacheRecup.getTribunal());
		iTacheDAO.deleteById(tacheSave.getIdTache());
		iTribunalDAO.deleteById(tribunalSave.getIdTribunal());
//		iAffaireDAO.deleteById(affaireSave.getIdAffaire());

	}

	@Test
	void RecuperTachesAvecTribunalAffaireUtilisateursPhases6() {
		// créer la liste d'utilisateur à save dans tache
		List<Utilisateur> utilisateurs = new ArrayList<>();
		Utilisateur utilisateursSave = iUtilisateurDAO
				.save(new Utilisateur("email", "nomUtilisateur", "prenomUtilisateur", "motDePasse"));
		utilisateurs.add(utilisateursSave);

		// phase
		Phase phaseSave = iphaseDAO.save(new Phase("phase1", ZonedDateTime.now(), ZonedDateTime.now()));

		// créer le tribunal à save dans tache
		Tribunal tribunal = new Tribunal("adresse", 10.10, 10.10, "region");
		Tribunal tribunalSave = iTribunalDAO.save(tribunal);
		// créer l'affaire à save
		Affaire affaire = new Affaire("reference", "titre", "description", 10);
		Affaire affaireSave = iAffaireDAO.save(affaire);
		// save la tache
		Tache tacheSave = iTacheDAO.save(tache);

     	// ajouter les paramètres
		tacheSave.setUtilisateurs(utilisateurs);
		tacheSave.setTribunal(tribunalSave);
		tacheSave.setAffaire(affaireSave);
		phaseSave.setTache(tacheSave);
		iphaseDAO.save(phaseSave);
		List<Phase> phases =iphaseDAO.recupererPhase(tacheSave.getIdTache());
		
		
		
		// save avec tout les paramètres
		Tache tacheRecup =iTacheDAO.save(tacheSave);
		assertEquals("reference", tacheRecup.getAffaire().getReference());
		assertEquals("phase1", phases.get(0).getNom());
		assertEquals("adresse", tacheRecup.getTribunal().getAdresse());
		assertEquals("email", tacheRecup.getUtilisateurs().get(0).getEmail());
		iTribunalDAO.deleteById(tribunalSave.getIdTribunal());
		iAffaireDAO.deleteById(affaireSave.getIdAffaire());
//		iUtilisateurDAO.deleteById(utilisateursSave.getIdUtilisateur());
		
	}



}
