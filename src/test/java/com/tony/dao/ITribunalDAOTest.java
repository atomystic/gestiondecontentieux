package com.tony.dao;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tony.entities.Tribunal;

import com.tony.repository.ITribunalDAO;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ITribunalDAOTest {
	@Autowired
	ITribunalDAO iTribunalDao;
	Tribunal tribunal;

	@BeforeEach
	void initialiserTribunal() {
		tribunal = new Tribunal("5 avenue Henri Becquerel", 10.10d, 10d, "Nouvelle-aquitaine");
	}

	@Test
	void ajouterTribunal1() {
		int tailleListeTribunalsAvant = iTribunalDao.findAll().size();
		Tribunal tribunalSave = iTribunalDao.save(tribunal);
		int tailleListeTribunalsApres = iTribunalDao.findAll().size();
		assertEquals(tailleListeTribunalsAvant + 1, tailleListeTribunalsApres);
//		iTribunalDao.deleteById(tribunalSave.getIdTribunal());

	}

//	@Test
//	void supprimerTribunal2() {
//
//		Tribunal tribunalSave = iTribunalDao.save(tribunal);
//		int tailleListeTribunalsAvant = iTribunalDao.findAll().size();
//		iTribunalDao.deleteById(tribunalSave.getIdTribunal());
//		int tailleListeTribunalsAprès = iTribunalDao.findAll().size();
//		assertEquals(tailleListeTribunalsAvant - 1, tailleListeTribunalsAprès);
//
//	}

}
