package com.tony.dao;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tony.entities.Affaire;
import com.tony.entities.Utilisateur;

import com.tony.repository.IUtilisateurDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class IUtilisateurDAOTest {

	@Autowired
	IUtilisateurDAO iUtilisateurDAO;
	Utilisateur utilisateur;

	@BeforeEach
	public void instanciationAffaire() {
		utilisateur = new Utilisateur("email", "nomUtilisateur", "prenomUtilisateur", "motDePasse");

	}

	@Test
	public void ajouterUtilisateur1() {
		int tailleListeUtilisateursAvant = iUtilisateurDAO.findAll().size();
		Utilisateur utilisateurSave = iUtilisateurDAO.save(utilisateur);
		int tailleListeUtilisateursApres = iUtilisateurDAO.findAll().size();
		assertEquals(tailleListeUtilisateursAvant + 1, tailleListeUtilisateursApres);
		iUtilisateurDAO.deleteById(utilisateurSave.getIdUtilisateur());

	}

//	@Test
//	void supprimerUtilisateur2() {
//
//		Utilisateur utilisateurSave = iUtilisateurDAO.save(utilisateur);
//		int tailleListeUtilisateursAvant = iUtilisateurDAO.findAll().size();
//		iUtilisateurDAO.deleteById(utilisateurSave.getIdUtilisateur());
//		int tailleListeUtilisateursAprès = iUtilisateurDAO.findAll().size();
//		assertEquals(tailleListeUtilisateursAvant - 1, tailleListeUtilisateursAprès);
//
//	}

	@Test
	void recupererUtilisateur3() {

		Utilisateur utilisateurSave = iUtilisateurDAO.save(utilisateur);
		Utilisateur utilisateurRecuperer = iUtilisateurDAO.findById(utilisateurSave.getIdUtilisateur()).get();
		assertNotEquals("null", utilisateurRecuperer.getNomUtilisateur());
//		iUtilisateurDAO.deleteById(utilisateurRecuperer.getIdUtilisateur());

	}

	@Test
	void updateUtilisateur4() {
		Utilisateur utilisateurSave = iUtilisateurDAO.save(utilisateur);
		utilisateurSave.setEmail("amail");
		Utilisateur utilisateurRecup = iUtilisateurDAO.save(utilisateurSave);
		assertEquals("amail", utilisateurRecup.getEmail());
//		iUtilisateurDAO.deleteById(utilisateurRecup.getIdUtilisateur());

	}
	
	

}
